import GoalStatus from "enums/GoalStatus";

interface IGoal {
  id?: number;
  title: string;
  description: string;
  date: Date;
  status: GoalStatus;
}

export default IGoal;