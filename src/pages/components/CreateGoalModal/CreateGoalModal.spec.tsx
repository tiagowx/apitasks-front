import { render, screen } from '@testing-library/react';
import CreateGoalModal from '.';


describe('GoalsTable component test', () => {
  
  test('renders empty', () => {
    // render
    render(<CreateGoalModal />)   
  });

  test('renders without crashing', () => {
    // render
    render(<CreateGoalModal />)
    
    // act
    
    // set query
    const button = screen.getByTestId("create-goal-modal-button");
        
    // expect
    expect(button).toBeInTheDocument();
    
  })
});