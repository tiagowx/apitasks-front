import { Add } from "@mui/icons-material";
import { Box, Button, MenuItem, Select, TextField, Typography } from "@mui/material"
import Modal from '@mui/material/Modal';
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { convertStatusToEnum, convertStatusToString } from "configs/helpers";
import GoalStatus from "enums/GoalStatus";
import { createGoal, getAllGoals } from "hooks/global/reducers/goalSlice";
import store from "hooks/global/store";
import IGoal from "interfaces/IGoal";
import { useState } from "react";
import { redirect } from "react-router-dom";
import ServiceTasks from "services/ServiceTasks";

const CreateGoalModal: React.FC = () => {
  //const api = new ServiceTasks();
  const [isOpen, setIsOpen] = useState(false)
  const [goal, setGoal] = useState<IGoal>({
    title: "",
    description: "",
    status: 0,
    date: new Date(),
  });

  const handlerOpenAndClose = () => setIsOpen(!isOpen);

  function handlerOnSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if ((goal === undefined) ||
      (goal.title.length === 0) ||
      (goal.description.length === 0) ||
      (goal.date === undefined) ||
      (goal.status === undefined)
    ) return;

    //api.createGoal(goal);
    store.dispatch(createGoal(goal));
    setTimeout(()=>{
      store.dispatch(getAllGoals());
    },1000)
  }

  return (
    <>
      <Button
        variant="contained"
        onClick={handlerOpenAndClose}
      >
        <Add />
        Criar novo
      </Button>
      <Modal
        open={isOpen}
        onClose={handlerOpenAndClose}
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        sx={{
          display:'flex',
          justifyContent:'center',
          alignItems:'center'
        }}
      >

        <Box
          component="form"
          onSubmit={handlerOnSubmit}
          sx={{
            position: 'absolute' as 'absolute',
            display: 'flex',
            flexDirection: 'column',
            width: 400,
            p: 2,
            bgcolor: '#fff',
            gap: '16px',
          }}
        >
          <Typography
            component="h3"
            variant="h5"
            sx={{
              fontWeight: 'bold',
              textAlign: 'center'
            }}
          >
            Criar Tarefa
          </Typography>
          <TextField
            label="Título"
            size="small"
            value={goal.title}
            placeholder="Título"
            onChange={e => setGoal({ ...goal, title: e.currentTarget.value })}
          >
          </TextField>
          <TextField
            label="Descrição"
            size="small"
            value={goal.description}
            placeholder="Descreva a taréfa"
            onChange={e => setGoal({ ...goal, description: e.currentTarget.value })}
          >
          </TextField>

          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              label="Data"
              value={goal.date}
              onChange={(newValue) => {
                setGoal({
                  ...goal,
                  date: new Date(new Date())
                })
              }}
              renderInput={(params) => <TextField size="small" {...params} />}
            />
          </LocalizationProvider>
          <Select
            label="status"
            size="small"
            value={goal.status}
            onChange={e =>
              setGoal({
                ...goal,
                status: convertStatusToEnum(e.target.value)
              })
            }>
            <MenuItem value={GoalStatus.Pendente}>
              {convertStatusToString(GoalStatus.Pendente)}
            </MenuItem>
            <MenuItem value={GoalStatus.Finalizado}>
              {convertStatusToString(GoalStatus.Finalizado)}
            </MenuItem>
          </Select>
          <Button type="submit" variant="contained" size="large">
            Criar
          </Button>
        </Box>
      </Modal>
    </>
  );
}

export default CreateGoalModal;