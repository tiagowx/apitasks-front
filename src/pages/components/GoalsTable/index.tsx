import { Delete, Edit, Add } from "@mui/icons-material";
import { Box, Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import { convertStatusToString } from "configs/helpers";
import IGoal from "interfaces/IGoal";

interface Props {
  goals: IGoal[];
}

const GoalsTable: React.FC<Props> = (props: Props) => {

  return (
    <Box sx={{
      display: 'flex',
      paddingX: '10%',
      maxHeight: '80vh',
      flexDirection: 'column'
    }}>
      <Table>
        <TableHead>
          <TableRow >
            <TableCell sx={{ fontWeight: "bold" }}>#</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Título</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Descrição</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Data</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.goals && props.goals.map((item, index) =>
            <TableRow key={index.toString().concat('-row')}>
              <TableCell >{index + 1}</TableCell>
              <TableCell>{item.title}</TableCell>
              <TableCell>{item.description}</TableCell>
              <TableCell >{new Date(item?.date || "").toLocaleDateString("pt-br")}</TableCell>
              <TableCell>{convertStatusToString(item.status)}</TableCell>
              <Box>
                <Button
                  variant="contained"
                  color="secondary"
                >
                  <Edit />
                </Button>
                <Button
                  variant="contained"
                  color="error">
                  <Delete />
                </Button>
              </Box>
            </TableRow>
          )}
        </TableBody>

      </Table>
    </Box>
  );
};

export default GoalsTable;