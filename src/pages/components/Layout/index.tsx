import { Box } from "@mui/material";
import Footer from "./Footer";
import Header from "./Header";

interface Props {
  children: JSX.Element;
}

const Layout: React.FC<Props> = ({ children }) => {
  return (
    <Box
      component="main"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        justifyContent: 'space-between',
      }}>
      <Header />
      <Box
        component="div"
        sx={{
          display: 'flex',
          flexDirection: 'column',
          paddingTop: "80px",
          height: '80vh'
        }}
      >
        {children}
      </Box>
      <Footer />
    </Box>
  );
}

export default Layout;