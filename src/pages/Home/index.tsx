import { Search } from "@mui/icons-material";
import { Dayjs } from "dayjs";
import { Box, Button, TextField } from "@mui/material";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import GoalStatus from "enums/GoalStatus";
import IGoal from "interfaces/IGoal";
import GoalsTable from "pages/components/GoalsTable";
import React, { useEffect, useState } from "react";
import ServiceTasks from "services/ServiceTasks";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import CreateGoalModal from "pages/components/CreateGoalModal";
import { useDispatch, useSelector } from "react-redux";
import { getAllGoals, selectAllGoals } from 'hooks/global/reducers/goalSlice'
import store from "hooks/global/store";


const Home: React.FC = () => {
  const goals: IGoal[] = useSelector(selectAllGoals);
  const [search, setSearch] = useState<string>("");
  const [date, setDate] = useState<Dayjs | null>(null);
  const [filters, setFilters] = useState<IGoal | undefined>(undefined);
  const [status, setStatus] = useState<GoalStatus>(1);

  // async function handlerGetAll() {
  //   setGoals(await api.getGoalsAll());
  //   setFilters({
  //     date: date?.toDate() || new Date(0),
  //     description: search,
  //     id: 0,
  //     status: GoalStatus.Pendente,
  //     title: search

  //   })
  //   console.log(goals);
  // }

  // async function handlerSearch(event: React.FormEvent<HTMLFormElement>) {
  //   event.preventDefault();
  //   if (search.length <= 0) {
  //     console.log(`Campo vazio ${search.valueOf}`);
  //     return;
  //   }

  //   setGoals(await api.getGoalsByTitle(search));
  //   console.log(goals);
  //   console.log(date?.toString());
  //   console.log(new Date().getDay().toString());
  // }

  useEffect(() => {
    if ((goals.length != store.getState().goals.length) || goals.length == 0) {
      setTimeout(() => {
        store.dispatch(getAllGoals());
        console.log(store.getState().goals.length);
      }, 3000);
    }
  }, [goals]);

  return (
    <Box
      component="section"
      sx={{
        maxHeight: '100%'
      }}>
      <Box
        component="form"
        //onSubmit={handlerSearch}
        sx={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            label="Data"
            value={date}
            onChange={(newValue) => { setDate(newValue) }}
            renderInput={(params) => <TextField size="small" {...params} />}
          />
        </LocalizationProvider>
        <TextField
          size="small"
          value={search}
          onChange={e => setSearch(e.currentTarget.value)}>
        </TextField>
        <Button type="submit">
          <Search />
        </Button>
      </Box>
      <CreateGoalModal />
      <Button variant="contained" onClick={() => store.dispatch(getAllGoals())}> Update</Button>
      <GoalsTable goals={goals} />
    </Box>
  );
}

export default Home;