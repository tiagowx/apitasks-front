const app = {
  "AppName":"Tasks",
  "configs": {
    baseURL : process.env.REACT_APP_URL_BASE,
  }
};

export default app;