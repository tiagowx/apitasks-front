import axios from 'axios';
import GoalStatus from 'enums/GoalStatus';
import IGoal from 'interfaces/IGoal';
import app from '../configs/app';
import { StatusCodes } from 'http-status-codes';

const instance = axios.create(app.configs);

class ServiceTasks {
  createGoal = async (goal: IGoal) => {
    if (!goal)
      return StatusCodes.BAD_REQUEST;

    const result = await instance.post("/Goal", {
      "title": goal.title,
      "description": goal.description,
      "date": goal.date,
      "status": goal.status,
    });

    if (result.status !== StatusCodes.CREATED)
      return StatusCodes.BAD_REQUEST

    return goal;
  }

  getGoalsAll = async () => {
    const result = await instance.get(`/Goal/ObterTodos`)
      .then((res) => res);
    if (result.status !== StatusCodes.OK) {
      console.log(result.status);
      return StatusCodes.FORBIDDEN;
    }
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByTitle = async (title: string) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, { params: { title } })
      .then((res) => res);

    if (result.status !== StatusCodes.OK) {
      console.log(StatusCodes.NOT_FOUND);
      return StatusCodes.NOT_FOUND;
    }

    const { data } = result;
    console.log(result.status);
    return data;
  }
  getGoalsByDescription = async (description: string) => {
    const result = await instance.get(`/Goal/ObterPorDescricao`, { params: { description } })
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByDate = async (date: Date) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, { params: { date } })
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByStatus = async (status: GoalStatus) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, { params: { status } })
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByFilters = async (goal: IGoal) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, { params: goal })
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  //update
  updateGoalById = async (id: number) => {
    instance.post('');

    return StatusCodes.OK;
  }
}

export default ServiceTasks;