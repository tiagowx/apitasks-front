import './App.css';
import { Provider } from 'react-redux';
import Pages from './pages';
import store from 'hooks/global/store';
import { getAllGoals } from 'hooks/global/reducers/goalSlice';


function App() {  

  return (
    <Provider store={store}>
      <Pages />
    </Provider>
  );
}

export default App;
