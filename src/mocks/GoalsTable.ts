import IGoal from "interfaces/IGoal";

const MockGoalsTable: IGoal[] = [
  {
      "id": 1,
      "title": "Tarefa 1",
      "description": "Primeira tarefa teste",
      "date": new Date("2022-11-16"),
      "status": 1
  } 
];

export default MockGoalsTable;