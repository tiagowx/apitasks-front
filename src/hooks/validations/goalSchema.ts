import { Fragment } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import GoalStatus from "enums/GoalStatus";

const goalSchema = Yup.object().shape({
  title: Yup.string()
    .required()
    .min(3, 'Não contem a quantidade mímina de caracteres.')
    .max(32, 'Execeu a quantidade máxima de caracteres.'),
  description: Yup.string()
    .required()
    .min(3, 'Não contem a quantidade mímina de caracteres.')
    .max(255, 'Execeu a quantidade máxima de caracteres.'),
  date: Yup.date()
    .required()
    .default(() => new Date())
    .min(0, 'Data inserida não é válida.'),
  status: Yup.mixed().oneOf(Object.values(GoalStatus))
    .required()
});

export default goalSchema;