import { StaticDatePicker } from "@mui/x-date-pickers";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import IGoal from "interfaces/IGoal";
import MockGoalsTable from "mocks/GoalsTable";
import ServiceTasks from "services/ServiceTasks";
import store from "../store";


const goalService = new ServiceTasks();

export const getAllGoals = createAsyncThunk('goals/getAll', async () => {
  return goalService.getGoalsAll();
});

export const createGoal = createAsyncThunk('goals/createOne',
  async (goal: IGoal) => {
    const result = await goalService.createGoal(goal);
    return result;
  });

export const goalSlice = createSlice({
  name: 'goals',
  initialState: {
    goals: [] as IGoal[]
  },
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(getAllGoals.fulfilled, (state, action) => {
        state.goals = action.payload;
      })
      .addCase(createGoal.fulfilled, (state, action) => {
       
       });
  }
});


export const selectAllGoals = (state: { goals: IGoal[]; }) => state.goals



export default goalSlice.reducer;


